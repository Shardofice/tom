package ru.tomlogic.tom8;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

/**
 * Created by Nexus on 08.04.2018.
 */

public class CommentLine implements Parcelable {

    private View ln;
    private View dl;
    private View tw;
    private View et;
    private String comment;


    public CommentLine(View ln, View dl, View tw, View et) {
        this.ln = ln;
        this.dl = dl;
        this.tw = tw;
        this.et = et;
    }


    protected CommentLine(Parcel in) {
        comment = in.readString();
    }

    public static final Creator<CommentLine> CREATOR = new Creator<CommentLine>() {
        @Override
        public CommentLine createFromParcel(Parcel in) {
            return new CommentLine(in);
        }

        @Override
        public CommentLine[] newArray(int size) {
            return new CommentLine[size];
        }
    };

    public View getLn() {
        return ln;
    }

    public void setLn(View ln) {
        this.ln = ln;
    }

    public View getDl() {
        return dl;
    }

    public void setDl(View dl) {
        this.dl = dl;
    }

    public View getTw() {
        return tw;
    }

    public void setTw(View tw) {
        this.tw = tw;
    }


    public View getEt() {
        return et;
    }

    public void setEt(View et) {
        this.et = et;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(comment);
    }
}
