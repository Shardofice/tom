package ru.tomlogic.tom8;


import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoteFragment extends Fragment implements View.OnClickListener {

    final String SAVED_TEXT = "saved_text";
    ArrayList<CommentLine> comments;
    private View rootView;
    String prComment;
    Button btnAdd;

    public NoteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_note, container, false);
        createButtons(rootView);

        Bundle args = getArguments();
        comments = (ArrayList<CommentLine>)args
                .getSerializable("LIST");


        //createButtons(rootView);
        if(!comments.isEmpty())updateCom();

        SharedPreferences sp = getActivity().getPreferences(MODE_PRIVATE);
        Integer length = sp.getInt(SAVED_TEXT, 0);

        for (Integer i = 0; i < length; i++) {
            //sp.getString(i.toString());

        }

        return rootView;
    }

    public void createButtons(View v) {
        btnAdd = ((Button) v.findViewById(R.id.btnAdd));
        btnAdd.setOnClickListener(this);
    }

    protected void loadComments() {
        SharedPreferences sp =  getActivity().getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        Map<String, ?> map= sp.getAll();
        for (int i = 0; i < map.size(); i++) {
            map.get(i);
            }
    }

    private void updateCom() {
        for (Integer i = 0; i < comments.size(); i++) {
            onBtnAdd(true,i);
        }
    }

    @SuppressLint("SetTextI18n")
    private void onBtnAdd(boolean refreshing, int num) {

        LinearLayout ln = new LinearLayout(getActivity());
        ln.setOrientation(LinearLayout.HORIZONTAL);
        ((LinearLayout) rootView.findViewById(R.id.scrl)).addView(ln);


        TextView tw = new TextView(getActivity());
        ln.addView(tw);
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) tw.getLayoutParams();
        lp.weight = 1;


        final EditText et = new EditText(getActivity());
        ln.addView(et);
        lp = (LinearLayout.LayoutParams) et.getLayoutParams();
        lp.weight = 300;
        et.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(et, 0);
        et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                for (Integer i = 0; i < comments.size(); i++) {
                    if(comments.get(i).getEt()==view)comments.get(i).setComment(et.getText().toString());}}});


        et.setText(prComment);
        Button dl = new Button(getActivity());
        ln.addView(dl);
        dl.setBackgroundResource(R.drawable.del);
        lp = (LinearLayout.LayoutParams) dl.getLayoutParams();
        lp.width = (int) (23 * getActivity().getResources().getDisplayMetrics().density);
        lp.height = (int) (23 * getActivity().getResources().getDisplayMetrics().density);
        lp.gravity = Gravity.CENTER;
        dl.setOnClickListener(this);


        if(!refreshing) {
            comments.add(new CommentLine(ln, dl, tw, et));
            tw.setText(String.valueOf(comments.size())+". ");
        } else {
            et.setText(comments.get(num).getComment());
            tw.setText(String.valueOf(num+1)+". ");
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAdd:
                onBtnAdd(false, 0);
                Log.d("---------","-----------");
                break;
            default:
                for (Integer i = 0; i < comments.size(); i++) {
                    if(comments.get(i).getDl()==v)comments.remove(comments.get(i));
                }

                LinearLayout n = (LinearLayout) v.getParent();
                n.removeAllViews();
                updateNum();
                break;
        }
    }

    private void updateNum() {
        for (Integer i = 0; i < comments.size(); i++) {
            ((TextView)comments.get(i).getTw()).setText(String.valueOf(i+1)+". ");
        }
    }
}
