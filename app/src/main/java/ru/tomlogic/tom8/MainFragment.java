package ru.tomlogic.tom8;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;

import android.os.CountDownTimer;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.util.ArrayList;



/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements View.OnClickListener, NumberPicker.OnValueChangeListener {

    int hr = 0, mn = 0;
    boolean btnClkEnabled = true;
    boolean need_up = false;
    String prComment;

    CountDownTimer countDownTimer;
    DrawerLayout drawer;
    NumberPicker np1;
    NumberPicker np2;
    ArrayList<CommentLine> comments;
    LinearLayout scroll;
    Button btnClk;
    Button btnAdd;
    Button btnBurger;
    Button btnStart;
    Button btnOk;
    Dialog d;
    View rootView;



    public MainFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer,  R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        KeyboardVisibilityEvent.setEventListener(
                getActivity(),
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        if(isOpen) {
                            rootView.findViewById(R.id.imageView).setVisibility(View.GONE);
                            rootView.findViewById(R.id.btnClk).setVisibility(View.GONE);}
                                else {
                            rootView.findViewById(R.id.imageView).setVisibility(View.VISIBLE);
                            rootView.findViewById(R.id.btnClk).setVisibility(View.VISIBLE);
                        }
                    }
                });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle args = getArguments();
        comments = (ArrayList<CommentLine>)args
                .getSerializable("LIST");


        rootView = inflater.inflate(R.layout.fragment_main, container, false);
        createButtons(rootView);
        if(!comments.isEmpty())updateCom();
        return rootView;
    }


    public void createButtons(View v) {
        btnClk = ((Button) v.findViewById(R.id.btnClk));
        btnClk.setOnClickListener(this);
        btnAdd = ((Button) v.findViewById(R.id.btnAdd));
        btnAdd.setOnClickListener(this);
        btnBurger = ((Button) v.findViewById(R.id.btnBurger));
        btnBurger.setOnClickListener(this);
        btnStart = ((Button) v.findViewById(R.id.btnStart));
        btnStart.setOnClickListener(this);
        scroll = ((LinearLayout) v.findViewById(R.id.scrl));


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnClk:
                onBtnClk();
                break;
            case R.id.btnAdd:
                onBtnAdd(false, 0);
                break;
            case R.id.btnStart:
                onBtnStart();
                break;
            case R.id.btnBurger:
                onBtnBurger();
                break;
            case R.id.btnOk:
                onBtnOk();
                break;
            default:
                for (Integer i = 0; i < comments.size(); i++) {
                    if(comments.get(i).getDl()==v)comments.remove(comments.get(i));
                }

                LinearLayout n = (LinearLayout) v.getParent();
                n.removeAllViews();

                updateNum();
                break;
        }


    }

    private void onBtnBurger() {
        drawer.openDrawer(GravityCompat.START);
    }

    private void onBtnStart() {
        if (hr == 0 && mn == 0) onClick(btnClk);
        else {
            if (btnStart.getText() == "STOP") {
                btnClkEnabled = true;
                countDownTimer.cancel();
                btnStart.setText("START");
            } else {
                btnStart.setText("STOP");
                btnClkEnabled = false;
                int time = hr * 60 + mn;
                countDownTimer = new CountDownTimer(time * 1000 + 700, 1000) {
                    @Override
                    public void onTick(long millis) {
                        if (need_up) {
                            //                      btnClk = ((Button) fragment.getView().findViewById(R.id.btnClk));
                            need_up = false;
                        }
                        if (mn == 0) {
                            hr--;
                            mn = 59;
                        } else mn--;
                        if (mn <= 9) btnClk.setText(hr + ":0" + mn);
                        else btnClk.setText(hr + ":" + mn);
                        //btnClk.setText("sec: " + (int) (millis / 1000));
                    }

                    @Override
                    public void onFinish() {
                        btnClkEnabled = true;
                        btnClk.setText("Fin!");
                        btnStart.setText("START");

                    }
                }.start();
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void onBtnAdd(boolean refreshing, int num) {

        LinearLayout ln = new LinearLayout(getActivity());
        ln.setOrientation(LinearLayout.HORIZONTAL);
        ((LinearLayout) rootView.findViewById(R.id.scrl)).addView(ln);


        TextView tw = new TextView(getActivity());
        ln.addView(tw);
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) tw.getLayoutParams();
        lp.weight = 1;


        final EditText et = new EditText(getActivity());
        ln.addView(et);
        lp = (LinearLayout.LayoutParams) et.getLayoutParams();
        lp.weight = 300;
        et.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(et, 0);
        et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                for (Integer i = 0; i < comments.size(); i++) {
                    if(comments.get(i).getEt()==view)comments.get(i).setComment(et.getText().toString());}}});


        et.setText(prComment);
        Button dl = new Button(getActivity());
        ln.addView(dl);
        dl.setBackgroundResource(R.drawable.del);
        lp = (LinearLayout.LayoutParams) dl.getLayoutParams();
        lp.width = (int) (23 * getActivity().getResources().getDisplayMetrics().density);
        lp.height = (int) (23 * getActivity().getResources().getDisplayMetrics().density);
        lp.gravity = Gravity.CENTER;
        dl.setOnClickListener(this);


        if(!refreshing) {
            comments.add(new CommentLine(ln, dl, tw, et));
            tw.setText(String.valueOf(comments.size())+". ");
        } else {
            et.setText(comments.get(num).getComment());
            tw.setText(String.valueOf(num+1)+". ");
        }

    }

    private void onBtnClk() {

        if (btnClkEnabled) {
            d = new Dialog(getActivity());
            d.setContentView(R.layout.np_dialog);
            np1 = d.findViewById(R.id.numberPicker1);
            np1.setMaxValue(59);
            np1.setMinValue(0);
            np1.setWrapSelectorWheel(true);
            np1.setOnValueChangedListener(this);
            np2 = d.findViewById(R.id.numberPicker2);
            np2.setMaxValue(59);
            np2.setMinValue(0);
            np2.setWrapSelectorWheel(true);
            np2.setOnValueChangedListener(this);
            btnOk = ((Button) d.findViewById(R.id.btnOk));
            btnOk.setOnClickListener(this);
            d.show();

        }
    }

    private void updateNum() {
        for (Integer i = 0; i < comments.size(); i++) {
            ((TextView)comments.get(i).getTw()).setText(String.valueOf(i+1)+". ");
        }
    }


    private void updateCom() {
        for (Integer i = 0; i < comments.size(); i++) {
            onBtnAdd(true,i);
        }
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        btnClk = ((Button) rootView.findViewById(R.id.btnClk));
        if (picker == np2) {
            hr = newVal;
            btnClk.setText(hr + ":" + mn);
        } else if (picker == np1) {
            mn = newVal;
            if (newVal <= 9) btnClk.setText(hr + ":0" + mn);
            else btnClk.setText(hr + ":" + mn);
        }
    }

    public void onBtnOk() {
        d.cancel();
    }



    @Override
    public void onDestroy() {

        super.onDestroy();

    }

}