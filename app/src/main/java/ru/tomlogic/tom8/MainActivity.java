package ru.tomlogic.tom8;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    final String SAVED_TEXT = "saved_text";

    Fragment fragment_main;
    Fragment fragment_notes;
    Fragment currentFragment;
    FragmentTransaction fragmentTransaction;
    ArrayList<CommentLine> comments;
    Bundle args;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        comments = new ArrayList<>();

        args = new Bundle();

        fragment_main = new MainFragment();
        args.putSerializable("LIST", comments);
        fragment_main.setArguments(args);
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame, fragment_main);
        fragmentTransaction.commit();
        currentFragment = fragment_main;

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.tomato) {
            fragmentTransaction = getFragmentManager().beginTransaction();
            if (currentFragment == fragment_notes)
                fragmentTransaction.setCustomAnimations(R.animator.sl_down, R.animator.sl_downn);
            fragmentTransaction.replace(R.id.frame, fragment_main);
            fragmentTransaction.commit();
            currentFragment = fragment_main;

        } else if (id == R.id.tasks) {


        } else if (id == R.id.notes) {
            if (fragment_notes == null) fragment_notes = new NoteFragment();
            fragment_notes.setArguments(args);
            fragmentTransaction = getFragmentManager().beginTransaction();
            if (currentFragment == fragment_main)
                fragmentTransaction.setCustomAnimations(R.animator.sl_upp, R.animator.sl_up);
            fragmentTransaction.replace(R.id.frame, fragment_notes);
            fragmentTransaction.commit();
            currentFragment = fragment_notes;

        } else if (id == R.id.settings) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStop() {

        SharedPreferences sp = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        if (!sp.contains(SAVED_TEXT)) ed.putInt(SAVED_TEXT, 0);
        Integer length = sp.getInt(SAVED_TEXT, 0);

        for (int i = 0; i < comments.size(); i++) {
            if (!Objects.equals(comments.get(i).getComment(), "")) {
                ed.putString(length.toString(), comments.get(i).getComment());
                length++;
            }
        }
        ed.putInt(SAVED_TEXT, length);
        ed.apply();

//        for (Integer i = 0; i < 100; i++) {
//            Log.d("=================", sp.getString(i.toString(), ""));
//        }
//        Log.d("=================", length.toString());

        super.onStop();
    }

}
